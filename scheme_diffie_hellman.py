#!/usr/bin/python
# -*- coding: utf-8 -*-
import crypto_math
import random

'''
 Diffie-Hellman Scheme 
 Generates two nums g and p (1 < g < p - 1) such as
 all nums from [1, p-1] can be represented by powers of g mod p
 g and p are public keys
 Users generates keys Xa, Xb, Xc, etc
 This keys are private for all users
 All users computes Y keys
 Ya = g^Xa mod p
 Yb = g^Xb mod p
      .... 
 etc
 This keys are public keys
 To get connection between two users they should computes
 Zab = Xa^Yb mod p & Zba = Ya^Zb mod p
 Zab == Zba
''' 


def is_sophie_germain_prime(p):
   #return (3**p - 1) % (2*p+1) == 0
    return crypto_math.mod_exp(3,p,2*p+1) == 1

def gen_sophie_germain_prime(max=1000000L):
    '''
    In number theory, a prime number p is a Sophie Germain prime
    if 2p + 1 is also prime.
    For example, 23 is a Sophie Germain prime because it is a prime
    and 2 × 23 + 1 = 47, and 47 is also a prime number.
    '''
    t = random.randint(1, max)
    if t % 2 == 0: t += 1
    while True: 
        if crypto_math.rmspp(t) and is_sophie_germain_prime(t) :
            return t
            break            
        t += 2
           
def generate_public_keys(private_keys):    
    q = gen_sophie_germain_prime(); 
    p = q * 2 + 1                  # p shoukd be sg prime 
    g = 0                 
    while crypto_math.mod_exp(g, q, p) != 1:
        g = random.randint(1, p)
    public_keys = [crypto_math.mod_exp(g,key,p) for key in private_keys]
    return (public_keys, p)


X = []
for i in xrange(0, 4): X.append(random.randint(0L, 10000000000L)) # generates private keys
Y = generate_public_keys(X)                                       # generates public keys

print X
print Y

print crypto_math.mod_exp(Y[0][0], X[1], Y[1])                   # computes Zab
print crypto_math.mod_exp(Y[0][1], X[0], Y[1])                   # computes Zba
