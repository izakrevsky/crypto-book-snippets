import crypto_math
import random

'''
RSA

The RSA keys are obtained as follows:
1. Choose two prime numbers p and q
2. Compute n=pq
3. Compute ?(n)=totient(p,q)
4. Choose e coprime to ?(n) such that gcd(e,n)=1
5. Compute d=modInverse(e,?(n))
6. e is the publickey; n is also made public; d is the privatekey

Encryption is as follows:
1. Size of data to be encrypted must be less than n
2. ciphertext=pow(plaintext,publickey,n)

Decryption is as follows:
1. Size of data to be encrypted must be less than 
'''

def keys(bits):
    """
    keys(bits) -> (public, private)
    Generate public and private RSA keys of the given size.
    """
    # Pragma: use a fixed e, the fourth Fermat prime (0b10000000000000001)
    e = 2**16+1
    while True:
        # Generate two large prime numbers p and q, n = pq and phi = (p-1)(q-1)
        s = bits / 2
        mask = 0b11 << (s - 2) | 0b1 # two highest and the lowest bit
        while True:
            p = random.getrandbits(s) | mask
            # Pragma: check p % e here to guarantee that phi and e are coprimes
            if p % e != 1 and crypto_math.rmspp(p): 
                break
        s = bits - s
        mask = 0b11 << (s - 2) | 0b1 # same as above, but maybe different s
        while True:
            q = random.getrandbits(s) | mask
            if q != p and q % e != 1 and crypto_math.rmspp(q): 
                break
        n = p * q
        phi = (p - 1) * (q - 1)
        # Pragma: e is chosen already and is relative prime to phi
        # Compute d, a modular multiplicative inverse to e (i.e. e*d % phi = 1)
        d = crypto_math.mod_inverse(e, phi)
        if d: # if not, the process will repeat
            break
    return (n, e), (n, d)
    
def rsa(message, key):
    """
    rsa(message, key) -> transformed message
    Apply RSA transformation to the message.
    """
    n, e = key
    if n < message:
        raise ValueError('the message is longer than the key')
    return crypto_math.mod_exp(message, e, n)        
