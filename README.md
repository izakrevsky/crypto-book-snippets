crypto-book-snippets
====================
This repository contains implementations of crypto algorithms discribed in the book "Рябко Б.Я., Фионов А.Н. - Криптографические методы защиты информации"

###Contains:

* Main crypto helpers
	* Rabin-Miller Strong Pseudoprime Test
	* Modular exponentation
	* Extended Euclidean Algorithm
	* Modular inverse
* Diffie-Hellman Scheme
* Shamir Cipher
* ElGamal Cipher
* RSA
* Algo Giant step Infant step

