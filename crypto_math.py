# -*- coding: utf-8 -*-
import math
import random

def mod_exp(base, exponent, modulus):
    """
    mod_exp(b, e, m) -> value of b**e % m
    Calculate modular exponentation using right-to-left binary method.
    http://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method
    """
    result = 1L
    while exponent > 0:
        if (exponent & 1) == 1:
            result = (result * base) % modulus
        exponent >>= 1
        base = (base * base) % modulus
    return result


def rmspp(number, attempts=28):
    """
    rmspp(n, attempts=28) -> True if n appears to be primary, else False
    rmspp: Rabin-Miller Strong Pseudoprime Test
    http://mathworld.wolfram.com/Rabin-MillerStrongPseudoprimeTest.html
    """
    if number < 2:
        return False
    if number == 2:
        return True
    if number % 2 == 0:
        return False
    # Given an odd integer n, let n = 2**r*s+1, with s odd... 
    s = number - 1
    r = 0
    while s % 2 == 0:
        r += 1
        s /= 2
    while attempts:
        # ... choose a random integer a with 1 <= a <= n-1
        a = random.randint(1, number-1)
	# Unless a**s % n != 1 ...
        if mod_exp(a, s, number) != 1:
            # ... and a**((2**j)*s) % n != -1 for some 0 <= j <= r-1 
            for j in range(0, r):
                if mod_exp(a, (2**j)*s, number) == number-1:
                    break
            else:
                return False
        attempts -= 1
        continue
    # A prime will pass the test for all a.
    return True

def egcd(a,b):
    # Extended Euclidean Algorithm
    # returns x, y, gcd(a,b) such that ax + by = gcd(a,b)
    u, u1 = 1, 0
    v, v1 = 0, 1
    while b:
        q = a // b
        u, u1 = u1, u - q * u1
        v, v1 = v1, v - q * v1
        a, b = b, a - q * b
    return u, v, a

def gcd(a,b):
    # 2.8 times faster than egcd(a,b)
    a,b=(b,a) if a<b else (a,b)
    while b:
        a,b=b,a%b
    return a    

def log2(int_num):
    # returns the base 2 logarithm for anumber, as long as it is not negative, or zero. 
    assert int_num>0
    return math.trunc(math.log(int_num)/math.log(2))
    
def mod_inverse(e,n):
    # d such that de = 1 (mod n)
    # e must be coprime to n
    # this is assumed to be true
    return egcd(e,n)[0]%n

def get_prime(max=1000000L):
    a = random.randint(1, max)        
    if a % 2 == 0: a += 1
    while not rmspp(a):
        a += 2
    return a
