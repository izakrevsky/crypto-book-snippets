import crypto_math
import random

'''    
 ElGamal Cipher 
 Generates two nums g and p (1 < g < p - 1) such as
 all nums from [1, p-1] can be represented by powers of g mod p
 g and p are public keys
 Users generates keys Ca, Cb, Cc, etc
 This keys are private for all users
 All users computes  keys
 Da = g^Ca mod p
 Db = g^Cb mod p
      .... 
 etc
 This keys are public keys
 To sent message between two users they should computes
 A user : r = g^k mod p
          e = m * Db^k mod p
 and sent (r, e) to B user
 B user calculates m` = e * r^(p-1-Cb) mod p
'''

    
def is_sophie_germain_prime(p):
   #return (3**p - 1) % (2*p+1) == 0
    return crypto_math.mod_exp(3,p,2*p+1) == 1

def gen_sophie_germain_prime(max=1000000L):
    '''
    In number theory, a prime number p is a Sophie Germain prime
    if 2p + 1 is also prime.
    For example, 23 is a Sophie Germain prime because it is a prime
    and 2 x 23 + 1 = 47, and 47 is also a prime number.
    '''
    t = random.randint(1, max)
    if t % 2 == 0: t += 1
    while True: 
        if crypto_math.rmspp(t) and is_sophie_germain_prime(t) :
            return t
            break            
        t += 2


q = gen_sophie_germain_prime(); 
p = q * 2 + 1                                          # p shoukd be sg prime 
g = 0                 
while crypto_math.mod_exp(g, q, p) != 1:            
    g = random.randint(1, p)
                                                       # p & g are public keys

c, d = [], []
[c.append(random.randint(1, p)) for i in xrange(0, 5)] # generating private keys
[d.append(crypto_math.mod_exp(g, ci, p)) for ci in c]  # computing public keys

m = 10                                                 # message 
k = random.randint(1, p-1)                             #  
r = crypto_math.mod_exp(g, k, p)                       # variables to sent
e = m * crypto_math.mod_exp(d[1], k, p)
mq = e * crypto_math.mod_exp(r, p-1-c[1], p)           # decoded message
