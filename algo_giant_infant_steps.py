import random
import math

'''
 y = a^x mod p
 a, p, y
 x - ?
 1. Gives two numbers m & k such as mk > p
 2. Calculate two sequences
     y, ay, a^2y ... a^(m-1)y  (mod p)
     a^m, a^(2m),  ... a^(km)  (mod p)
 3. Find i and j such as a ^ (im) == a^jy
 4. x = im - j
''' 

def step_giant_step_infant(a, p, y):

    m, k = 0, 0          
    while m*k < p:
        m = random.randint(math.trunc(math.sqrt(p)), p)
        k = random.randint(math.trunc(math.sqrt(p)), p)
        
    first = lambda x : x[0]    
    step_infant = sorted(([(y*(a**i) % p, i) for i in xrange(0, m)]), key=first)
    step_giant = sorted(([(a**(i*m) % p, i) for i in xrange(1, k+1)]), key=first)

    flag = False
    for j in xrange(0, len(step_giant)):
        if flag: break
        for i in xrange(0, len(step_infant)):
            if step_infant[i][0] == step_giant[j][0]:
                pair = (step_infant[i][-1], step_giant[j][-1])
                if pair[0]*m-(pair[1]+1) < 0: continue
                else:                     
                    flag = True
                    break   
                        
    return pair[0]*m-(pair[1]+1)	     


