import random
import crypto_math
# -*- coding: utf-8 -*-

'''
 Shamir cipher
 Public key is big prime number (p)
 Both members generate two private keys c and d
 such as : cd mod (p-1) == 1
  Step 1 : A computes  x1 =  m^ca mod p  and sent it to B
  Step 2 : B computes  x2 = x1^cb mod p  and sent it to A
  Step 3 : A computes  x3 = x2^da mod p  and sent it to B
  Step 4 : B computes  x4 = x3^db mod p 
 At finish x4 == m
'''

def get_c_key(p, max=1000000L):
    # gcd(c_key, p-1) should be 1
    t = random.randint(1, max)
    if t % 2 == 0: t += 1
    while crypto_math.gcd(p-1, t) != 1:
        t += 2 # we need only odd nums
    return t

public_key = crypto_math.get_prime()       # generating public key

keys_A, keys_B = [], []                    # lists of users keys
keys_A.append(get_c_key(public_key))       # generating ca key
keys_A.append(crypto_math.mod_inverse(keys_A[-1], (public_key - 1)))  # generating da key
keys_B.append(get_c_key(public_key))       # generating cb key
keys_B.append(crypto_math.mod_inverse(keys_B[-1], (public_key - 1)))  # generating db key

print keys_A
print keys_B

message = 5                                # original message
x = []
x.append(crypto_math.mod_exp(message, keys_A[0], public_key))    # step1 
x.append(crypto_math.mod_exp(x[-1], keys_B[0], public_key))      # step2
x.append(crypto_math.mod_exp(x[-1], keys_A[1], public_key))      # step3
x.append(crypto_math.mod_exp(x[-1], keys_B[1], public_key))      # step4
# x[-1] == message
print x

